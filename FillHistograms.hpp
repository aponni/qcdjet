#ifndef _FILL_HISTOGRAMS_HPP
#define _FILL_HISTOGRAMS_HPP

#include <unordered_map>
#include <forward_list>
#include <vector>
#include <set>
#include <string>

#include <TLorentzVector.h>

#include "DataWrapper.hpp"
#include "HistogramWrapper.hpp"
#include "Trigger.hpp"

/**
 * Class for filling histograms.
 *
 * This class handles everything related to histogram filling. That is, this class
 * can read the data files specified in the configuration file "qcdjet.conf". Also
 * Monte Carlo data is loaded. After loading, data is looped through and processed
 * into histograms, profiles and graphs. All this is done by calling Fill().
 * After that, the filled histograms can be written to a ROOT-file by calling Write().
 */
class FillHistograms {
public:
	FillHistograms();
	~FillHistograms();
	FillHistograms(const FillHistograms&) = delete; 				// Copy constructor disabled
	FillHistograms& operator=(const FillHistograms&) = delete;			// Assignment operator disabled
	void Fill();
	void Write(std::string filename);
private:
	void ApplyCorrections(float rho, float area, TLorentzVector& p4, bool isMC);
	void InitializeHistograms();
	void Loop(DataWrapper& data, bool isMC);
	void FillDataAdditional();
	void FillMCAdditional();
	void FillDataMass(DataWrapper& data, const std::set<Trigger> firedTriggers);
	void FillMCMass(DataWrapper& data);

	std::unordered_map<int,std::unordered_map<int,HistogramWrapper>> mHistogramWrappers;
	std::unordered_map<int,HistogramWrapper> mMonteCarloWrappers;
};

#endif
