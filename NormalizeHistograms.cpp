#include <sstream>
#include <iostream>

#include <TFile.h>
#include <TH1D.h>
#include <TProfile.h>
#include <TGraphErrors.h>
#include <TROOT.h>

#include "Config.hpp"
#include "NormalizeHistograms.hpp"
#include "Utils.hpp"

/**
 * Empty constructor.
 */
NormalizeHistograms::NormalizeHistograms() {
}

/**
 * Empty destructor.
 */
NormalizeHistograms::~NormalizeHistograms() {
}

/**
 * A top level method for normalizing histograms in a file.
 *
 * This method calls private submethods that first load the histograms,
 * profiles and graphs from a ROOT-file and then normalizes those.
 *
 * @param filename Path to the ROOT-file that contains the histograms etc.
 */
void NormalizeHistograms::Normalize(std::string filename) {

	LoadFromFile(filename);

	NormalizeData();
	NormalizeMC();
}

/**
 * Writes histograms to disk.
 *
 * This method writes the normalized histograms to a ROOT-file.
 * Subdirectory structure of the original file is preserved.
 *
 * @param filename Path to the ROOT-file that will be written to.
 */
void NormalizeHistograms::Write(std::string filename) {

	TFile file(filename.c_str(), "recreate");
	for (auto& array : mHistogramWrappers) {
		for (auto& elem : array.second) {
			elem.second.Write();
		}
	}
	for (auto& elem : mMonteCarloWrappers) {
		elem.second.Write();
	}
	file.Close();
}

/**
 * Loads histograms, profiles and graphs from a ROOT-file.
 *
 * Loads the histograms, profiles and graphs from a file written by
 * the FillHistograms-class. The directory structure of the original input
 * file will be preserved.
 *
 * @param filename Path to the ROOT-file containing the histograms that will be normalized.
 */
void NormalizeHistograms::LoadFromFile(std::string filename) {

	Config& config = Config::GetInstance();
	const std::vector<double> etaBins = config.GetDoubleVector("etaBins");
	const std::vector<std::string> triggerNames = config.GetStringVector("triggerNames");
	const std::vector<int> useTriggers = config.GetIntegerVector("useTriggers");

	std::stringstream ss;
	TFile file(filename.c_str(), "read");
	gROOT->cd();
	for (unsigned int iEta = 0; iEta != etaBins.size()-1; iEta++) {

		const double minEta = etaBins[iEta];
		const double maxEta = etaBins[iEta+1];
		ss.str("");
		ss << "Eta_" << minEta << "-" << maxEta;
		const std::string directory = ss.str();
		auto& mcWrapper = mMonteCarloWrappers[iEta];
		mcWrapper.SetDirectory(directory+"/mc");
		mcWrapper.AddObject(file.Get((directory+"/mc/massHistogram").c_str())->Clone());
		mcWrapper.AddObject(file.Get((directory+"/mc/massResHistogram").c_str())->Clone());
		mcWrapper.AddObject(file.Get((directory+"/mc/massProfile").c_str())->Clone());
		mcWrapper.AddObject(file.Get((directory+"/mc/genMassHistogram").c_str())->Clone());
		mcWrapper.AddObject(file.Get((directory+"/mc/genMassProfile").c_str())->Clone());
		for (unsigned int iTrg = 0; iTrg != triggerNames.size(); iTrg++) {

			if (Utils::Exists(useTriggers, (int)iTrg)) {
				ss.str("");
				ss << directory << "/" << triggerNames[iTrg];
				const std::string subDirectory = ss.str();
				auto& wrapper = mHistogramWrappers[iEta][iTrg];
				wrapper.SetDirectory(ss.str());
				wrapper.AddObject(file.Get((subDirectory+"/massHistogram").c_str())->Clone());
				wrapper.AddObject(file.Get((subDirectory+"/massProfile").c_str())->Clone());
			}
		}
	}
	file.Close();
}

/**
 * Normalizes the loaded data.
 *
 * Takes the previously loaded data histograms, profiles and graphs and normalizes them.
 * The normalization converts the counts from input histograms to cross sections.
 * The normalized histogram wrappers share the same directory structure as the original wrappers.
 */
void NormalizeHistograms::NormalizeData() {

	Config& config = Config::GetInstance();
	const std::vector<double> luminosities = config.GetDoubleVector("luminosities");
	const std::vector<double> etaBins = config.GetDoubleVector("etaBins");

	// Iterate through all mass histograms
	for (unsigned int iEta = 0; iEta != etaBins.size()-1; iEta++) {
		const double etaWidth = etaBins[iEta+1] - etaBins[iEta];
		for (auto& elem : mHistogramWrappers[iEta]) {
			auto& wrapper = elem.second;
			auto massHistogram = (TH1D*)wrapper.Get("massHistogram");
			auto massProfile = (TProfile*)wrapper.Get("massProfile");
			auto massGraph = new TGraphErrors(0);
			massGraph->SetName("massGraph");
			wrapper.AddObject(massGraph);
			// Normalize each bin
			for (int bin = 1; bin != massHistogram->GetNbinsX()+1; bin++) {
				// Compute normalization factor
				const double normalization = massHistogram->GetBinWidth(bin) * etaWidth * luminosities[elem.first];
				massHistogram->SetBinContent(bin, massHistogram->GetBinContent(bin) / normalization);
				massHistogram->SetBinError(bin, massHistogram->GetBinError(bin) / normalization);
				// Fill graph
				const double x = massProfile->GetBinContent(bin);
				const double y = massHistogram->GetBinContent(bin);
				if (x> 0.0 && y > 0.0) {
					const int point = massGraph->GetN();
					massGraph->SetPoint(point, x, y);
					massGraph->SetPointError(point, massProfile->GetBinError(bin), massHistogram->GetBinError(bin));
				}
			}
		}
	}
}

/**
 * Normalizes the loaded Monte Carlo data.
 *
 * The same thing as the NormalizeData()-method except for Monte Carlo data
 * instead of real data. How this differs from the method intended for data
 * by also normalizing the generator data (prefixed with "gen").
 */
void NormalizeHistograms::NormalizeMC() {

	Config& config = Config::GetInstance();
	const std::vector<double> etaBins = config.GetDoubleVector("etaBins");

	// Iterate through all mass histograms
	for (unsigned int iEta = 0; iEta != etaBins.size()-1; iEta++) {
		const double etaWidth = etaBins[iEta+1] - etaBins[iEta];
		auto& wrapper = mMonteCarloWrappers[iEta];
		auto massHistogram = (TH1D*)wrapper.Get("massHistogram");
		auto massProfile = (TProfile*)wrapper.Get("massProfile");
		auto massGraph = new TGraphErrors(0); massGraph->SetName("massGraph");
		wrapper.AddObject(massGraph);
		auto genMassHistogram = (TH1D*)wrapper.Get("genMassHistogram");
		auto genMassProfile = (TProfile*)wrapper.Get("genMassProfile");
		auto genMassGraph = new TGraphErrors(0); genMassGraph->SetName("genMassGraph");
		wrapper.AddObject(genMassGraph);
		// Normalize each bin
		for (int bin = 1; bin != massHistogram->GetNbinsX()+1; bin++) {
			// Compute normalization factor
			const double normalization = massHistogram->GetBinWidth(bin) * etaWidth / 2500.0;
			massHistogram->SetBinContent(bin, massHistogram->GetBinContent(bin) / normalization);
			massHistogram->SetBinError(bin, massHistogram->GetBinError(bin) / normalization);
			genMassHistogram->SetBinContent(bin, genMassHistogram->GetBinContent(bin) / normalization);
			genMassHistogram->SetBinError(bin, genMassHistogram->GetBinError(bin) / normalization);
			// Fill graphs
			const double x = massProfile->GetBinContent(bin);
			const double y = massHistogram->GetBinContent(bin);
			if (x > 0.0 && y > 0.0) {
				const int point = massGraph->GetN();
				massGraph->SetPoint(point, x, y);
				massGraph->SetPointError(point, massProfile->GetBinError(bin), massHistogram->GetBinError(bin));
			}
			const double genX = genMassProfile->GetBinContent(bin);
			const double genY = genMassHistogram->GetBinContent(bin);
			if (genX > 0.0 && genY > 0.0) {
				const int point = genMassGraph->GetN();
				genMassGraph->SetPoint(point, genX, genY);
				genMassGraph->SetPointError(point, genMassProfile->GetBinError(bin), genMassHistogram->GetBinError(bin));
			}
		}
	}
}

