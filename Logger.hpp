#ifndef _LOGGER_HPP
#define _LOGGER_HPP

#include <forward_list>
#include <iostream>
#include <fstream>
#include <string>

enum LoggerLevel { DEBUG, WARNING, ERROR };

class Logger {
public:
	Logger() {
		mStream = &std::cout;
		mOwnedStream = false;
		mLevel = ERROR;
	}
	Logger(std::ostream* stream, LoggerLevel verbosity = ERROR) {
		mStream = stream;
		mOwnedStream = false;
		mLevel = verbosity;
	}
	Logger(std::string filename, LoggerLevel verbosity = ERROR) {
		mStream = new std::ofstream(filename.c_str());
		if (mStream->fail()) {
			mStream = &std::cout;
			mOwnedStream = false;
		} else {
			mOwnedStream = true;
		}
		mLevel = verbosity;
	}
	virtual ~Logger() {
		if (mOwnedStream) {
			delete mStream;
		}
	}
	void Flush() {
		mStream->flush();
	}
	virtual void Log(std::string msg, LoggerLevel level) {
		if (level < mLevel) return;
		(*mStream) << msg;
	}
protected:
	std::ostream* mStream;
	bool mOwnedStream;
	LoggerLevel mLevel;
};

class CompositeLogger {
public:
	~CompositeLogger() {
		for (Logger* logger : mLoggers) {
			delete logger;
		}
		mLoggers.clear();
	}
	CompositeLogger(const CompositeLogger&) = delete;
	CompositeLogger& operator=(const CompositeLogger&) = delete;
	static CompositeLogger& GetInstance() {
		static CompositeLogger instance;
		return instance;
	}
	void AddLogger(Logger* logger) {
		mLoggers.push_front(logger);
	}
	void Flush() {
		for (Logger* logger : mLoggers) {
			logger->Flush();
		}
	}
	void Log(std::string msg, LoggerLevel verbosity) {
		for (Logger* logger : mLoggers) {
			logger->Log(msg, verbosity);
		}
	}
	void Log(int msg, LoggerLevel verbosity) {
		Log(std::to_string(msg), verbosity);
	}
	void Log(double msg, LoggerLevel verbosity) {
		Log(std::to_string(msg), verbosity);
	}
	void Log(long int msg, LoggerLevel verbosity) {
		Log(std::to_string(msg), verbosity);
	}
	void Log(long long int msg, LoggerLevel verbosity) {
		Log(std::to_string(msg), verbosity);
	}
	void Log(unsigned long int msg, LoggerLevel verbosity) {
		Log(std::to_string(msg), verbosity);
	}
	// Convenient operator overloads
	CompositeLogger& operator<<(LoggerLevel level) {
		mCurrentLevel = level;
		return *this;
	}
	CompositeLogger& operator<<(std::string msg) {
		Log(msg, mCurrentLevel);
		return *this;
	}
	CompositeLogger& operator<<(int msg) {
		Log(msg, mCurrentLevel);
		return *this;
	}
	CompositeLogger& operator<<(double msg) {
		Log(msg, mCurrentLevel);
		return *this;
	}
	CompositeLogger& operator<<(long int msg) {
		Log(msg, mCurrentLevel);
		return *this;
	}
	CompositeLogger& operator<<(long long int msg) {
		Log(msg, mCurrentLevel);
		return *this;
	}
	CompositeLogger& operator<<(unsigned long msg) {
		Log(msg, mCurrentLevel);
		return *this;
	}
private:
	CompositeLogger() {
		mCurrentLevel = ERROR;
	}
	LoggerLevel mCurrentLevel;
	std::forward_list<Logger*> mLoggers;
};

#endif

