#include <sstream>

#include <TFile.h>
#include <TROOT.h>

#include "CombineHistograms.hpp"
#include "Config.hpp"
#include "Utils.hpp"

/**
 * Combines the histograms, profiles and graphs in a given file.
 *
 * This method reads the input file, extracts the histograms, profiles
 * and graphs inside it and combines them trigger-wise for each eta bin.
 * The resulting histogram wrappers will be in the "mCombinedWrappers"
 * hashmap.
 *
 * @param filename Path to the ROOT-file containing the input histograms.
 */
void CombineHistograms::Combine(std::string filename) {

	LoadFromFile(filename);

	Config& config = Config::GetInstance();
	const std::vector<double> etaBins = config.GetDoubleVector("etaBins");
	const std::vector<std::string> triggerNames = config.GetStringVector("triggerNames");

	gROOT->cd();
	std::stringstream ss;
	for (unsigned int iEta = 0; iEta != etaBins.size()-1; iEta++) {

		// Generate directory name for combined histograms
		const double minEta = etaBins[iEta];
		const double maxEta = etaBins[iEta+1];
		ss.str("");
		ss << "Eta_" << minEta << "-" << maxEta;
		const std::string directory = ss.str();
		// A little rain dancing to avoid warnings caused by overlapping histogram names
		if (gDirectory->GetDirectory(directory.c_str()) == nullptr) {
			gDirectory->mkdir(directory.c_str());
		}
		gDirectory->cd(directory.c_str());

		std::unordered_map<int,TH1D*> massHistograms;
		std::unordered_map<int,TProfile*> massProfiles;
		for (auto& elem : mHistogramWrappers[iEta]) {
			auto& wrapper = elem.second;
			massHistograms[elem.first] = (TH1D*)wrapper.Get("massHistogram");
			massProfiles[elem.first] = (TProfile*)wrapper.Get("massProfile");
		}
		// Combine histograms
		TH1D* combinedMassHistogram = CombineTriggerHistograms("massHistogram", "Mass spectrum histogram", massHistograms);
		TProfile* combinedMassProfile = CombineTriggerProfiles("massProfile", "", massProfiles);
		TGraphErrors* combinedMassGraph = new TGraphErrors(0);
		combinedMassGraph->SetName("massGraph");
		// Fill combined graph
		for (int bin = 1; bin != combinedMassHistogram->GetNbinsX(); bin++) {
			const double x = combinedMassProfile->GetBinContent(bin);
			const double y = combinedMassHistogram->GetBinContent(bin);
			if (x > 0.0 && y > 0.0) {
				const int point = combinedMassGraph->GetN();
				combinedMassGraph->SetPoint(point, combinedMassProfile->GetBinContent(bin), combinedMassHistogram->GetBinContent(bin));
				combinedMassGraph->SetPointError(point, combinedMassProfile->GetBinError(bin), combinedMassHistogram->GetBinError(bin));
			}
		}

		// Initialize wrapper and fill it with content
		auto& wrapper = mCombinedWrappers[iEta];
		wrapper.SetDirectory(directory+"/data");
		wrapper.AddObject(combinedMassHistogram);
		wrapper.AddObject(combinedMassProfile);
		wrapper.AddObject(combinedMassGraph);

		gDirectory->cd("..");
	}
}

/**
 * Writes the combined histograms to disk.
 *
 * This method writes the combined histograms, profiles and graphs built in the
 * Combine()-method to a new ROOT-file. In addition to the combined data histograms,
 * also the Monte Carlo-histograms are written to disk. The originally read
 * data histograms will not be written to the output file.
 *
 * @param filename Path to the output ROOT-file.
 */
void CombineHistograms::WriteHistograms(std::string filename) {

	TFile file(filename.c_str(), "recreate");
	for (auto& elem : mCombinedWrappers) {
		elem.second.Write();
	}
	for (auto& elem : mMonteCarloWrappers) {
		elem.second.Write();
	}
	file.Close();
}

/**
 * Combines mass histograms from multiple triggers into one histogram.
 *
 * Takes a hashmap of histograms that represent the mass histograms for each trigger
 * in the current eta bin. Then cuts and pastes those histograms at the given points
 * and builds a new histogram that represents all those triggers.
 * The combining happens at points defined in the configuration parameter "combineAtMass".
 *
 * @param name Name of the combined histogram.
 * @param title Title of the combined histogram.
 * @param triggerHistograms A hashmap containing the histograms for each trigger.
 *
 * @return The combined histogram.
 */
TH1D* CombineHistograms::CombineTriggerHistograms(std::string name, std::string title, std::unordered_map<int,TH1D*> triggerHistograms) {

	const std::vector<double> combineAtMass = Config::GetInstance().GetDoubleVector("combineAtMass");
	// Note that all histograms have the same x-axis
	const int numBins = triggerHistograms.begin()->second->GetNbinsX();
	const double* axis = triggerHistograms.begin()->second->GetXaxis()->GetXbins()->GetArray();
	TH1D* combined = new TH1D(name.c_str(), title.c_str(), numBins, axis);
	for (int bin = 1; bin < numBins+1; bin++) {
		
		const double centerMass = combined->GetBinCenter(bin);
		// Check from which trigger the value should be taken
		const int trigger = Utils::FindBin(combineAtMass, centerMass);
		if (triggerHistograms.count(trigger) > 0) {
			combined->SetBinContent(bin, triggerHistograms[trigger]->GetBinContent(bin));
			combined->SetBinError(bin, triggerHistograms[trigger]->GetBinError(bin));
		}
	}
	return combined;
}

/**
 * Combines mass profiles from multiple triggers into one profile.
 *
 * Takes a hashmap of profiles that represent the mass profile for each trigger
 * in the current eta bin. Then cuts and pastes those profiles at the given points
 * and builds a new profile that represents all those triggers.
 * The combining happens at points defined in the configuration parameter "combineAtMass".
 *
 * @param name Name of the combined profile.
 * @param title Title of the combined profile.
 * @param triggerProfiles A hashmap containing the profiles for each trigger.
 *
 * @return The combined profile.
 */
TProfile* CombineHistograms::CombineTriggerProfiles(std::string name, std::string title, std::unordered_map<int,TProfile*> triggerProfiles) {

	const std::vector<double> combineAtMass = Config::GetInstance().GetDoubleVector("combineAtMass");
	// All profiles have the same x-axis
	const int numBins = triggerProfiles.begin()->second->GetNbinsX();
	const double* axis = triggerProfiles.begin()->second->GetXaxis()->GetXbins()->GetArray();
	// Create new profile for the combined result
	TProfile* combined = new TProfile(name.c_str(), title.c_str(), numBins, axis);
	for (int bin = 1; bin < numBins+1; bin++) {

		const double centerMass = combined->GetBinCenter(bin);
		// Check from which trigger the value should be taken to the combined profile
		const int trigger = Utils::FindBin(combineAtMass, centerMass);
		if (triggerProfiles.count(trigger) > 0) {
			combined->SetBinContent(bin, triggerProfiles[trigger]->GetBinContent(bin)*triggerProfiles[trigger]->GetBinEntries(bin));
			combined->SetBinEntries(bin, triggerProfiles[trigger]->GetBinEntries(bin));
			combined->SetBinError(bin, triggerProfiles[trigger]->GetBinError(bin));
		}
	}
	return combined;
}

/**
 * Loads histograms, profiles and graphs from a ROOT-file.
 *
 * Loads the histograms, profiles and graphs from a file written by
 * the NormalizeHistograms-class. The directory structure of the output file
 * will be the same for the Monte Carlo-data, but real data will have it's
 * trigger subdirectories replaced with a single "data"-subdirectory.
 *
 * @param filename Path to the ROOT-file containing the histograms that will be combined.
 */
void CombineHistograms::LoadFromFile(std::string filename) {

	Config& config = Config::GetInstance();
	const std::vector<double> etaBins = config.GetDoubleVector("etaBins");
	const std::vector<std::string> triggerNames = config.GetStringVector("triggerNames");
	const std::vector<int> useTriggers = config.GetIntegerVector("useTriggers");

	std::stringstream ss;
	TFile file(filename.c_str(), "read");
	gROOT->cd();
	for (unsigned int iEta = 0; iEta != etaBins.size()-1; iEta++) {

		const double minEta = etaBins[iEta];
		const double maxEta = etaBins[iEta+1];
		ss.str("");
		ss << "Eta_" << minEta << "-" << maxEta;
		const std::string directory = ss.str();
		auto& mcWrapper = mMonteCarloWrappers[iEta];
		mcWrapper.SetDirectory(directory+"/mc");
		mcWrapper.AddObject(file.Get((directory+"/mc/massHistogram").c_str())->Clone());
		mcWrapper.AddObject(file.Get((directory+"/mc/massResHistogram").c_str())->Clone());
		mcWrapper.AddObject(file.Get((directory+"/mc/massProfile").c_str())->Clone());
		mcWrapper.AddObject(file.Get((directory+"/mc/massGraph").c_str())->Clone());
		mcWrapper.AddObject(file.Get((directory+"/mc/genMassHistogram").c_str())->Clone());
		mcWrapper.AddObject(file.Get((directory+"/mc/genMassProfile").c_str())->Clone());
		mcWrapper.AddObject(file.Get((directory+"/mc/genMassGraph").c_str())->Clone());
		for (unsigned int iTrg = 0; iTrg != triggerNames.size(); iTrg++) {

			if (Utils::Exists(useTriggers, (int)iTrg)) {
				ss.str("");
				ss << directory << "/" << triggerNames[iTrg];
				const std::string subDirectory = ss.str();
				auto& wrapper = mHistogramWrappers[iEta][iTrg];
				wrapper.SetDirectory(subDirectory);
				wrapper.AddObject(file.Get((subDirectory+"/massHistogram").c_str())->Clone());
				wrapper.AddObject(file.Get((subDirectory+"/massProfile").c_str())->Clone());
				wrapper.AddObject(file.Get((subDirectory+"/massGraph").c_str())->Clone());
			}
		}
	}
	file.Close();
}

