#include <fstream>
#include <algorithm>
#include <stdexcept>

#include "Config.hpp"
#include "Logger.hpp"

Config::Config() {

	std::string configFile = "qcdjet.conf";
	Load(configFile);
	SanityCheck(); 		// Throws exception if fails
}

Config& Config::GetInstance() {

	static Config instance;
	return instance;
}

bool Config::GetBoolean(std::string option) {

	return mBooleanOptions[option];
}

int Config::GetInteger(std::string option) {

	return mIntegerOptions[option];
}

double Config::GetDouble(std::string option) {

	return mDoubleOptions[option];
}

std::string Config::GetString(std::string option) {

	return mStringOptions[option];
}

std::vector<bool> Config::GetBooleanVector(std::string option) {

	return mBooleanVectorOptions[option];
}

std::vector<int> Config::GetIntegerVector(std::string option) {

	return mIntegerVectorOptions[option];
}

std::vector<double> Config::GetDoubleVector(std::string option) {

	return mDoubleVectorOptions[option];
}

std::vector<std::string> Config::GetStringVector(std::string option) {

	return mStringVectorOptions[option];
}

void Config::SanityCheck() {

	// chainName exists
	if (mStringOptions["chainName"].empty()) {
		throw std::runtime_error("Option \"chainName\" (string) has to be specified.");
	}
	// dataFileList exists
	if (mStringVectorOptions["dataFileList"].empty()) {
		throw std::runtime_error("Option \"dataFileList\" (vector string) has to be specified.");
	}
	// mcFileList exists
	if (mStringVectorOptions["mcFileList"].empty()) {
		throw std::runtime_error("Option \"mcFileList\" (string) has to be specified.");
	}
	// numMassBins exists
	if (mIntegerOptions["numMassBins"] <= 0) {
		throw std::runtime_error("Option \"numMassBins\" (int) has to be positive.");
	}
	auto massRange = mDoubleVectorOptions["massRange"];
	// massRange exists
	if (massRange.empty()) {
		throw std::runtime_error("Option \"massRange\" (vector double) has to be specified.");
	}
	// massRange contains 2 elements
	if (massRange.size() != 2) {
		throw std::runtime_error("Option \"massRange\" (vector double) has to have exactly 2 elements.");
	}
	// massRange's elements are positive
	if (massRange[0] <= 0.0 || massRange[1] <= 0.0) {
		throw std::runtime_error("Option \"massRange\" (vector double) must have massRange[0]>0 and massRange[1]>0.");
	}
	// massRange's low limit is lower than high limit
	if (massRange[1] < massRange[0]) {
		throw std::runtime_error("Option \"massRange\" (vector double) must have massRange[0] < massRange[1].");
	}
	auto etaBins = mDoubleVectorOptions["etaBins"];
	// etaBins exists
	if (etaBins.empty()) {
		throw std::runtime_error("Option \"etaBins\" (vector double) has to be specified.");
	}
	// etaBins has at least 2 elements
	if (etaBins.size() < 2) {
		throw std::runtime_error("Option \"etaBins\" (vector double) has to have at least 2 elements.");
	}
	// etaBins' elements are non-negative
	for (int eta : etaBins) {
		if (eta < 0.0) {
			throw std::runtime_error("Elements in \"etaBins\" (vector double) must be non-negative.");
		}
	}
	// etaBins' elements are strictly increasing
	for (unsigned int i = 0; i < etaBins.size()-1; i++) {
		if (etaBins[i] >= etaBins[i+1]) {
			throw std::runtime_error("Elements in \"etaBins\" (vector double) must form a strictly increasing array of values.");
		}
	}
	// triggerNames exists
	auto triggers = mStringVectorOptions["triggerNames"];
	if (triggers.empty()) {
		throw std::runtime_error("Option \"triggerNames\" (vector string) has to be specified.");
	}
	// useTriggers exists
	auto useTriggers = mIntegerVectorOptions["useTriggers"];
	if (useTriggers.empty()) {
		throw std::runtime_error("Option \"useTriggers\" (vector integer) has to be specified.");
	}
	// Elements in useTriggers are in accepted range
	if (std::any_of(useTriggers.begin(), useTriggers.end(), [&triggers] (int useIndex) { return useIndex < 0 || useIndex >= (int)triggers.size(); })) {
		throw std::runtime_error("Elements in \"useTriggers\" (vector integer) must be in the range [0,triggerNames.sizw()).");
	}
	auto combine = mDoubleVectorOptions["combineAtMass"];
	// combineAtMass exists
	if (combine.empty()) {
		throw std::runtime_error("Option \"combineAtMass\" (vector double) has to be specified.");
	}
	// combineAtMass.size == triggerNames.size()+1
	if (combine.size() != triggers.size()+1) {
		throw std::runtime_error("Option \"combineAtMass\" (vector double) has to have exactly triggerNames.size()+1 elements.");
	}
	// combineAtMass' elements are strictly increasing
	for (unsigned int i = 0; i < combine.size()-1; i++) {
		if (combine[i] >= combine[i+1]) {
			throw std::runtime_error("Elements in \"combineAtMass\" (vector double) must form a strictly increasing array of values.");
		}
	}
	// luminosities exist
	auto luminosities = mDoubleVectorOptions["luminosities"];
	if (luminosities.empty()) {
		throw std::runtime_error("Option \"luminosities\" (vector double) has to be specified.");
	}
	// luminosities.size() == triggerNames.size()
	if (luminosities.size() != triggers.size()) {
		throw std::runtime_error("Option \"luminosities\" (vector double) has to have exactly triggerNames.size() elements.");
	}
}

void Config::Load(std::string confFile) {

	std::ifstream file;
	file.open(confFile.c_str());
	if (!file.is_open()) {
		std::cout << "Cannot open file " << confFile << " for reading.\n";
		return;
	}

	static std::vector<char> ignoredCharacters = { '=', ';', '\"', '\'' };
	std::string line, type, option;
	std::stringstream ss;
	while (std::getline(file, line)) {
		// Remove ignored characters
		for (char ignore : ignoredCharacters) {
			std::replace(line.begin(), line.end(), ignore, ' ');
		}
		// Check if line is empty
		if (line.empty()) continue;
		ss.str(line);
		ss.clear();
		ss >> type;
		// Check if line is comment
		if (type.substr(0,2) == "//") continue;
		// Rewind stringstream
		ss.seekg(std::ios_base::beg);
		if (type == "vector") {
			if (!ParseVectorOption(ss)) continue;
		} else {
			if (!ParseOption(ss)) continue;
		}
	}
}

bool Config::ParseOption(std::stringstream& ss) {

	CompositeLogger& logger = CompositeLogger::GetInstance();

	std::string type, option, value;
	ss >> type;
	ss >> option;
	ss >> value;
	if (type == "bool") {
		mBooleanOptions[option] = (value == "true");
	} else if (type == "int") {
		mIntegerOptions[option] = std::stoi(value);
	} else if (type == "double") {
		mDoubleOptions[option] = std::stod(value);
	} else if (type == "string") {
		mStringOptions[option] = value;
	} else {
		logger << WARNING << "Unrecognized option type \"" << type << "\"\n"; 	// TODO: Implement logger-singleton
		return false;
	}
	logger << DEBUG << "New option: " << type << " " << option << " = " << value << "\n";
	return true;
}

bool Config::ParseVectorOption(std::stringstream& ss) {

	CompositeLogger& logger = CompositeLogger::GetInstance();

	std::string type, option, value;
	ss >> type; 		// Skip "vector"
	ss >> type;
	ss >> option;
	logger << DEBUG << "New option: vector " << type << " " << option << " = { ";
	while (ss.good()) {
		try {
			ss >> value;
			if (type == "bool") {
				mBooleanVectorOptions[option].push_back(value == "true");
			} else if (type == "int") {
				mIntegerVectorOptions[option].push_back(std::stoi(value));
			} else if (type == "double") {
				mDoubleVectorOptions[option].push_back(std::stod(value));
			} else if (type == "string") {
				mStringVectorOptions[option].push_back(value);
			} else {
				logger << WARNING << "Unrecognized option type \"" << type << "\"\n";
				return false;
			}
		} catch (std::invalid_argument& ia) {
			break;
		}
		logger << DEBUG << value << " ";
	}
	logger << DEBUG << "}\n";
	return true;
}

