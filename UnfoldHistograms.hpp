#ifndef _UNFOLD_HISTOGRAMS_HPP
#define _UNFOLD_HISTOGRAMS_HPP

#include <string>
#include <tuple>
#include <unordered_map>

#include <TF1.h>
#include <TH1D.h>
#include <TGraphErrors.h>

#include "HistogramWrapper.hpp"

double MassResolution(double mass, double eta);
double SmearedAnsatzKernel(double* x, double* p);
double SmearedAnsatz(double* x, double* p);

/**
 * Class for unfolding histograms.
 *
 * This class reads the previously combined histograms, profiles and
 * graphs from a ROOT-file. Uses the iterative/bayesian/D'Agostini
 * method provided in the RooUnfold-framework.
 */
class UnfoldHistograms {
public:
	UnfoldHistograms();
	~UnfoldHistograms();
	void Unfold(std::string filename);
	void Write(std::string filename);
private:
	void UnfoldDataOrMC(bool isMC);
	void LoadFromFile(std::string filename);
	std::tuple<TH1D*,TGraphErrors*> UnfoldMassHistogram(TH1D* massHist, TH1D* nloHist, double minEta, double maxEta);

	std::unordered_map<int,HistogramWrapper> mHistogramWrappers;
	std::unordered_map<int,HistogramWrapper> mMonteCarloWrappers;
};

#endif

