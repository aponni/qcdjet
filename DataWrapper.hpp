#ifndef _DATA_WRAPPER_HPP
#define _DATA_WRAPPER_HPP

#include <string>
#include <vector>

#include <TBranch.h>
#include <TChain.h>

/**
 * Class that wraps various data fields into a single structure.
 *
 * This is intended to act as an additional layer of abstraction
 * as well as encapsulate the reading of data and the setup of 
 * branches into a prettier frame.
 */
class DataWrapper {
public:
	DataWrapper(std::vector<std::string> inputFiles, bool isMC);
	~DataWrapper();
	void GetEntry(long int entry);
	long int GetEntries();

	// Variables
	int numJets;
	int numGoodPVs;
	int run;
	int event;
	int lumisect;
	float weight;
	float rho;
	float radius[100];
	std::vector<int> triggerDecision;
	double p4x[100]; 			// TODO: Fix ugly hardcoded array lengths
	double p4y[100];
	double p4z[100];
	double p4t[100];
	double genP4x[100];
	double genP4y[100];
	double genP4z[100];
	double genP4t[100];
	float area[100];

	// Branches
	TBranch* numBranch = nullptr;
	TBranch* nvtxBranch = nullptr;
	TBranch* runBranch = nullptr;
	TBranch* eventBranch = nullptr;
	TBranch* lumisectBranch = nullptr;
	TBranch* rhoBranch = nullptr;
	TBranch* radiusBranch = nullptr;
	TBranch* weightBranch = nullptr;
	TBranch* triggerBranch = nullptr;
	TBranch* l1PrescaleBranch = nullptr;
	TBranch* hltPrescaleBranch = nullptr;
	TBranch* p4xBranch = nullptr;
	TBranch* p4yBranch = nullptr;
	TBranch* p4zBranch = nullptr;
	TBranch* p4tBranch = nullptr;
	TBranch* genP4xBranch = nullptr;
	TBranch* genP4yBranch = nullptr;
	TBranch* genP4zBranch = nullptr;
	TBranch* genP4tBranch = nullptr;
	TBranch* areaBranch = nullptr;
private:
	void BuildChain(std::vector<std::string> inputFile);
	void ConnectVariables();
	void EnableBranches(bool isMC);
	TChain* mChain;
};

#endif

