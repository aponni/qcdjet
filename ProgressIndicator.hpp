#ifndef _PROGRESS_INDICATOR_HPP
#define _PROGRESS_INDICATOR_HPP

#include <string>
#include <chrono>

class ProgressIndicator {

public:
	ProgressIndicator(int totalProgress);
	void Start();
	void SetProgress(int progress);
	int GetElapsed() const;
	int GetEstimate() const;
	std::string GetProgressBar() const;
private:
	int mTotalProgress;
	int mProgress;
	std::chrono::time_point<std::chrono::system_clock> mStartTime;
};

#endif

