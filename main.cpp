#include <iostream>
#include <string>

#include <TROOT.h>
#include <TChain.h>

#include "CombineHistograms.hpp"
#include "Config.hpp"
#include "FillHistograms.hpp"
#include "Logger.hpp"
#include "NormalizeHistograms.hpp"
#include "UnfoldHistograms.hpp"

TROOT root(" ", " ");

int main(int argc, char* argv[]) {

	// Initialize logging
	CompositeLogger& logger = CompositeLogger::GetInstance();
	logger.AddLogger(new Logger("log.txt", DEBUG));
	logger.AddLogger(new Logger(&std::cout, ERROR));

	// Execute the analysis chain
/*	FillHistograms h;
	h.Fill();
	h.Write("output.root");

	NormalizeHistograms n;
	n.Normalize("output.root");
	n.Write("normalized.root");

	CombineHistograms c;
	c.Combine("normalized.root");
	c.Write("combined.root");
*/
	UnfoldHistograms u;
	u.Unfold("combined.root");
	u.Write("unfolded.root");
}

