#include <sstream>
#include <iomanip>
#include <sys/ioctl.h>

#include "ProgressIndicator.hpp"

ProgressIndicator::ProgressIndicator(int totalProgress) {

	mTotalProgress = totalProgress;
}

void ProgressIndicator::Start() {

	mStartTime = std::chrono::system_clock::now();
	mProgress = 0;
}

void ProgressIndicator::SetProgress(int progress) {

	mProgress = progress;
}

int ProgressIndicator::GetElapsed() const {

	auto now = std::chrono::system_clock::now();
	return std::chrono::duration_cast<std::chrono::seconds>(now - mStartTime).count();
}

int ProgressIndicator::GetEstimate() const {

	const int elapsed = GetElapsed();
	return int(elapsed * float(mTotalProgress / mProgress));
}

std::string ProgressIndicator::GetProgressBar() const {

	// Getting terminal width
	struct winsize w;
	ioctl(0, TIOCGWINSZ, &w);
	const int terminalWidth = w.ws_col;

	// State information
	std::string state = "RUNNING";

	// Percentage
	std::stringstream percentage;
	percentage << std::setprecision(2) << 100.0 * mProgress / mTotalProgress << "%";

	// Elapsed
	std::string elapsedUnit = "s";
	int elapsed = GetElapsed();
	if (elapsed > 60) {
		elapsedUnit = "m";
		elapsed /= 60;
	} else if (elapsed > 60) {
		elapsedUnit = "h";
		elapsed /= 60;
	}

	// Estimate
	std::string estimateUnit = "s";
	int estimate = GetEstimate();
	if (estimate > 60) { 			// Find a suitable unit
		estimateUnit = "m";
		estimate /= 60;
	} else if (estimate > 60) {
		estimateUnit = "h";
		estimate /= 60;
	}
	std::stringstream remaining;
	remaining << "Time: " << elapsed << elapsedUnit << "/" << estimate << estimateUnit;

	// Assemble the progress bar
	std::string bar = " " + state + " " + percentage.str() + " " + remaining.str();
	
	// Fill remaining space with blanks
	std::stringstream blanks;
	for (int i = bar.length(); i < terminalWidth; i++) {
		blanks << " ";
	}

	return bar + blanks.str();
}
