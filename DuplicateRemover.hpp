#ifndef _DUPLICATE_REMOVER_HPP
#define _DUPLICATE_REMOVER_HPP

#include <map>
#include <unordered_map>
#include <vector>

#include "Config.hpp"

class DuplicateRemover {
public:
	virtual ~DuplicateRemover() {}
	virtual bool AddIfUnique(int run, int ls, int event) = 0;
	virtual long unsigned int GetNumDuplicates() = 0;
};

class HashedRemover : public DuplicateRemover {
public:
	~HashedRemover() {}
	inline bool AddIfUnique(int run, int ls, int event) {
		auto& element = mElements[event][ls][run];
		if (element) {
			// run-ls-event-combo already added
			mNumDuplicates++;
			return false;
		} else {
			element = true;
			return true;
		}
	}
	inline long unsigned int GetNumDuplicates() {
		return mNumDuplicates;
	}
private:
	long unsigned int mNumDuplicates = 0;
	std::unordered_map<int,std::unordered_map<int,std::unordered_map<int,bool>>> mElements;
};

class MappedRemover : public DuplicateRemover {
public:
	~MappedRemover() {}
	inline bool AddIfUnique(int run, int ls, int event) {
		auto element = mElements[run][ls][event];
		if (element) {
			// run-ls-event-combo already added
			mNumDuplicates++;
			return false;
		} else {
			element = true;
			return true;
		}
	}
	inline long unsigned int GetNumDuplicates() {
		return mNumDuplicates;
	}
private:
	long unsigned int mNumDuplicates = 0;
	std::map<int,std::map<int,std::map<int,bool>>> mElements;
};

#endif

