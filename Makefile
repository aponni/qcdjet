ROOTCFLAGS = $(shell root-config --cflags)
ROOTLIBS   = $(shell root-config --libs)
ROOTGLIBS  = $(shell root-config --glibs)

ROOTINCDIR = $(shell root-config --incdir)
ROOTBINDIR = $(shell root-config --bindir)

IDIR=.
ODIR=obj
LDIR=.
LIBS=$(ROOTLIBS) -L/afs/cern.ch/user/a/aponni/private/test/RooUnfold-1.1.1 -lRooUnfold

CC=g++
CFLAGS=-I$(IDIR) $(ROOTCFLAGS) -std=c++11 -Wall
OFLAGS=-O3 -march=native -ffast-math
DFLAGS=-g
SRC_EXT=cpp cc

_DEPS=CombineHistograms.hpp Config.hpp DataWrapper.hpp DrawPlots.hpp DuplicateRemover.hpp FillHistograms.hpp HistogramWrapper.hpp Logger.hpp NormalizeHistograms.hpp ProgressIndicator.hpp Trigger.hpp UnfoldHistograms.hpp Utils.hpp CondFormats/JetMETObjects/interface/FactorizedJetCorrector.h CondFormats/JetMETObjects/interface/JetCorrectionUncertainty.h CondFormats/JetMETObjects/interface/JetCorrectorParameters.h CondFormats/JetMETObjects/interface/SimpleJetCorrectionUncertainty.h CondFormats/JetMETObjects/interface/SimpleJetCorrector.h
DEPS=$(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ=CombineHistograms.o Config.o DataWrapper.o DrawPlots.o FillHistograms.o HistogramWrapper.o main.o NormalizeHistograms.o ProgressIndicator.o UnfoldHistograms.o CondFormats/JetMETObjects/src/FactorizedJetCorrector.o CondFormats/JetMETObjects/src/JetCorrectionUncertainty.o CondFormats/JetMETObjects/src/JetCorrectorParameters.o CondFormats/JetMETObjects/src/SimpleJetCorrectionUncertainty.o CondFormats/JetMETObjects/src/SimpleJetCorrector.o CondFormats/JetMETObjects/src/Utilities.o
OBJ=$(patsubst %,$(ODIR)/%,$(_OBJ))

EXEC=qcdjet

release: CFLAGS+=$(OFLAGS)
debug: CFLAGS+=$(DFLAGS)

define compile_rule
$(ODIR)/%.o: %.$1 $(DEPS)
	$$(CC) -c -o $$@ $$< $$(CFLAGS)
endef
$(foreach EXT,$(SRC_EXT),$(eval $(call compile_rule,$(EXT))))

all: $(OBJ)
	$(CC) -o $(EXEC) $^ $(CFLAGS) $(LIBS)

release: all
debug: all

.PHONY=clean

clean:
	rm -f $(ODIR)/*.o $(EXEC) $(ODIR)/CondFormats/JetMETObjects/src/*.o

