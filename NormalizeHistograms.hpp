#ifndef _NORMALIZE_HISTOGRAMS_HPP
#define _NORMALIZE_HISTOGRAMS_HPP

#include <string>
#include <unordered_map>

#include "HistogramWrapper.hpp"

/**
 * Class for normalizing histograms.
 *
 * This class reads histograms, profiles and graphs from a ROOT-file,
 * normalizes them to cross sections and writes them to a new ROOT-file.
 */
class NormalizeHistograms {
public:
	NormalizeHistograms();
	~NormalizeHistograms();
	NormalizeHistograms(const NormalizeHistograms&) = delete;
	NormalizeHistograms& operator=(const NormalizeHistograms&) = delete;
	void Normalize(std::string filename);
	void Write(std::string filename);
private:
	void LoadFromFile(std::string filename);
	void NormalizeData();
	void NormalizeMC();
	std::unordered_map<int,std::unordered_map<int,HistogramWrapper>> mHistogramWrappers;
	std::unordered_map<int,HistogramWrapper> mMonteCarloWrappers;
};

#endif

