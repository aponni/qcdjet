#ifndef UTILS_HPP
#define UTILS_HPP

#include <algorithm>
#include <cmath>
#include <string>
#include <sstream>
#include <vector>

#include <Trigger.hpp>

namespace Utils {

	template <typename T>
	inline int ElementIndex(const std::vector<T>& vec, const T& elem) {
		return std::find(vec.begin(), vec.end(), elem) - vec.begin();
	}

	template <typename T>
	inline bool Exists(const std::vector<T>& vec, const T& elem) {
		return std::find(vec.begin(), vec.end(), elem) != vec.end();
	}

	inline int FindBin(const std::vector<double>& vec, double val) {
		auto it = std::find_if(vec.begin()+1, vec.end(), [val](double current) { return val < current; });
		return it - vec.begin() - 1;
	}

	inline Trigger IndexToTrigger(int trigIndex) {
		if (trigIndex < 7) return jt40;
		else if (trigIndex < 14) return jt80;
		else if (trigIndex < 21) return jt140;
		else if (trigIndex < 28) return jt200;
		else if (trigIndex < 35) return jt260;
		else if (trigIndex < 42) return jt320;
		else return jt400;
	}

	inline std::vector<double> LinSpace(double low, double high, int numBins) {
		std::vector<double> bins;
		const double step = (high-low)/numBins;
		for (int i = 0; i <= numBins; i++) {
			bins.push_back(low + i * step);
		}
		return bins;
	}

	inline std::vector<double> LogSpace(double low, double high, int numBins) {
		std::vector<double> bins = LinSpace(std::log10(low), std::log10(high), numBins);
		for (double& x : bins) {
			x = std::pow(10.0, x);
		}
		return bins;
	}

	inline std::vector<std::string> SplitLine(std::string line, const std::vector<char>& tokens) {

		// Change all tokens in line to white space
		// Note: this algorithm splits the string at white spaces by default
		for (char token : tokens) {
			std::replace(line.begin(), line.end(), token, ' ');
		}

		static std::stringstream ss;
		ss.clear();
		ss.str(line);

		// Read all words in a vector
		std::string word;
		std::vector<std::string> result;
		while (ss >> word) {
			result.push_back(word);
		}
		return result;
	}

	inline Trigger StringToTrigger(const std::string triggerName) {
		if (triggerName == "jt40") return jt40;
		else if (triggerName == "jt80") return jt80;
		else if (triggerName == "jt140") return jt140;
		else if (triggerName == "jt200") return jt200;
		else if (triggerName == "jt260") return jt260;
		else if (triggerName == "jt320") return jt320;
		else return jt400;
	}

	inline std::string TriggerToString(const Trigger trigger) {
		if (trigger == jt40) return "jt40";
		else if (trigger == jt80) return "jt80";
		else if (trigger == jt140) return "jt140";
		else if (trigger == jt200) return "jt200";
		else if (trigger == jt260) return "jt260";
		else if (trigger == jt320) return "jt320";
		else return "jt400";
	}

	template <typename T>
	inline std::string VectorToString(const std::vector<T>& vec) {

		std::stringstream ss;
		ss << "[ ";
		for (const T& element : vec) {
			ss << element << " ";
		}
		ss << "]";
		return ss.str();
	}
}

#endif
