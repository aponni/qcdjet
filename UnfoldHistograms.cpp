#include <cmath>
#include <sstream>
#include <vector>

#include <TFile.h>
#include <TH2D.h>
#include <TGraphErrors.h>
#include <TMath.h>
#include <TMatrixD.h>
#include <TROOT.h>

#include "RooUnfold-1.1.1/src/RooUnfold.h"
#include "RooUnfold-1.1.1/src/RooUnfoldBayes.h"
#include "RooUnfold-1.1.1/src/RooUnfoldBinByBin.h"
#include "RooUnfold-1.1.1/src/RooUnfoldSvd.h"
#include "RooUnfold-1.1.1/src/RooUnfoldResponse.h"

#include "Config.hpp"
#include "Logger.hpp"
#include "UnfoldHistograms.hpp"
#include "Utils.hpp"

/**
 * Empty constructor.
 */
UnfoldHistograms::UnfoldHistograms() {
}

/**
 * Empty destructor.
 */
UnfoldHistograms::~UnfoldHistograms() {
}

/**
 * A top level method for unfolding histograms in a file.
 *
 * This method delegates work to private submethods that are used
 * to load the histograms etc from disk and carry out the unfolding.
 *
 * @param filename Path to the ROOT-file that contains the histograms etc.
 */
void UnfoldHistograms::Unfold(std::string filename) {

	LoadFromFile(filename);

	UnfoldDataOrMC(true);
	UnfoldDataOrMC(false);
}

TF1* kernel = nullptr;
bool isMCJer = false; 		// This is the uglies thing ever

/**
 * Method that unfolds all histograms (data or MC) that are loaded in memory.
 *
 * Iterates through all input histogram wrappers and calls the unfolding method
 * for each one. The resulting unfolded histograms and graphs are added
 * to the histogram wrapper.
 *
 * @param isMC Whether we are unfolding Monte Carlo or real data.
 */
void UnfoldHistograms::UnfoldDataOrMC(bool isMC) {

	Config& config = Config::GetInstance();
	std::vector<double> etaBins = config.GetDoubleVector("etaBins");
	std::stringstream ss;

	isMCJer = isMC;
	for (unsigned int iEta = 0; iEta != etaBins.size()-1; iEta++) {
		const double minEta = etaBins[iEta];
		const double maxEta = etaBins[iEta+1];
		ss.str("");
		ss << "Eta_" << minEta << "-" << maxEta;
		const std::string directory = ss.str();
		// This rain dance with directories will avoid the memory leak warnings
		if (gDirectory->GetDirectory(directory.c_str()) == nullptr) {
			gDirectory->mkdir(directory.c_str());
		}
		gDirectory->cd(directory.c_str());
		// Read mass histogram from file
		auto& target = isMC ? mMonteCarloWrappers[iEta] : mHistogramWrappers[iEta];
		TH1D* massHistogram = (TH1D*)(isMC ? target.Get("massHistogram") : target.Get("massHistogram"));
		TH1D* genMassHistogram = (TH1D*)mMonteCarloWrappers[iEta].Get("genMassHistogram");
		auto unfolded = UnfoldMassHistogram(massHistogram, genMassHistogram, minEta);
		// Add to result
		target.AddObject(std::get<0>(unfolded));
		target.AddObject(std::get<1>(unfolded));
		gDirectory->cd("..");
	}
}

/**
 * Writes histograms to disk.
 *
 * This method writes the input histograms to disk along with the unfolded
 * histograms and graphs. The original directory structure is preserved,
 * that is, each eta bin is in it's own subdirectory.
 *
 * @param filename Path to the ROOT-file that will be written to.
 */
void UnfoldHistograms::Write(std::string filename) {

	TFile file(filename.c_str(), "recreate");
	for (auto& elem : mHistogramWrappers) {
		// Write histograms
		elem.second.Write();
	}
	for (auto& elem : mMonteCarloWrappers) {
		elem.second.Write();
	}
	file.Close();
}

/**
 * Loads histograms, profiles and graphs from a ROOT-file.
 *
 * Loads the histograms, profiles and graphs from a file written by
 * the FillHistograms-class. The directory structure of the original input
 * file will be preserved.
 *
 * @param filename Path to the ROOT-file containing the histograms that will be normalized.
 */
void UnfoldHistograms::LoadFromFile(std::string filename) {

	Config& config = Config::GetInstance();
	const std::vector<double> etaBins = config.GetDoubleVector("etaBins");

	std::stringstream ss;
	TFile file(filename.c_str(), "read");
	gROOT->cd();
	for (unsigned int iEta = 0; iEta != etaBins.size()-1; iEta++) {

		const double minEta = etaBins[iEta];
		const double maxEta = etaBins[iEta+1];
		ss.str("");
		ss << "Eta_" << minEta << "-" << maxEta;
		const std::string directory = ss.str();
		HistogramWrapper mcWrapper(directory+"/mc");
		mcWrapper.AddObject(file.Get((directory+"/mc/massHistogram").c_str())->Clone());
		mcWrapper.AddObject(file.Get((directory+"/mc/massResHistogram").c_str())->Clone());
		mcWrapper.AddObject(file.Get((directory+"/mc/massProfile").c_str())->Clone());
		mcWrapper.AddObject(file.Get((directory+"/mc/massGraph").c_str())->Clone());
		mcWrapper.AddObject(file.Get((directory+"/mc/genMassHistogram").c_str())->Clone());
		mcWrapper.AddObject(file.Get((directory+"/mc/genMassProfile").c_str())->Clone());
		mcWrapper.AddObject(file.Get((directory+"/mc/genMassGraph").c_str())->Clone());
		mMonteCarloWrappers[iEta] = mcWrapper;

		HistogramWrapper wrapper(directory+"/data");
		wrapper.AddObject(file.Get((directory+"/data/massHistogram").c_str())->Clone());
		wrapper.AddObject(file.Get((directory+"/data/massProfile").c_str())->Clone());
		wrapper.AddObject(file.Get((directory+"/data/massGraph").c_str())->Clone());
		mHistogramWrappers[iEta] = wrapper;
	}
	file.Close();
}

/**
 * Computes the mass resolution at a given mass and eta.
 *
 * @param mass Mass for which the resolution will be computed.
 * @param eta Eta for which...
 *
 * @return Mass resolution for the given mass and eta.
 */
double MassResolution(double mass, double eta) {

	static Config& config = Config::GetInstance();
	static std::vector<std::vector<double>> vpar = { config.GetDoubleVector("massVParams0"),
							 config.GetDoubleVector("massVParams1"),
							 config.GetDoubleVector("massVParams2"),
							 config.GetDoubleVector("massVParams3"),
							 config.GetDoubleVector("massVParams4"),
							 config.GetDoubleVector("massVParams5") };
	static std::vector<std::vector<double>> kpar = { config.GetDoubleVector("massKParams0"),
							 config.GetDoubleVector("massKParams1"),
							 config.GetDoubleVector("massKParams2"),
							 config.GetDoubleVector("massKParams3"),
							 config.GetDoubleVector("massKParams4"),
							 config.GetDoubleVector("massKParams5") };
	static const int numBins = vpar.size();

	const int iEta = std::min(numBins-1, int(std::abs(eta) / 0.5));
	const int iY = std::min(5, iEta); 		// What?
	double resolution = std::sqrt(std::pow(vpar[iY][0]/mass, 2.0) + std::pow(vpar[iY][1], 2.0)/mass
						+ std::pow(vpar[iY][2], 2.0));
	if (!isMCJer) {
		resolution *= kpar[iY][0];
	}
	return resolution;
}

/**
 * Function for the smeared ansatz used in the unfolding.
 *
 * @param x Pointer to the function's variable.
 * @param p Array of the function's parameters.
 *
 * @return Value of the ansatz function.
 */
double SmearedAnsatz(double* x, double* p) {

	if (kernel == nullptr) {
		kernel = new TF1("smearedAnsatzKernel", SmearedAnsatzKernel, 1.0, 1000.0, 6);
	}

	const double mass = x[0];
	const double eta = p[0];
	const double resolution = MassResolution(mass, eta+1.0e-3) * mass;
	const double sigma = std::min(resolution, 0.30);
	double minMass = std::max(1.0, mass / (1.0 + 4.0 * sigma)); 		// maxMass*(1+4*sigma)=mass
	double maxMass = std::min(8000.0, mass / (1.0 - 2.0 * sigma));		// minMass*(1-2*sigma)=mass

	const double parameters[6] = { mass, p[0], p[1], p[2], p[3] };
	kernel->SetParameters(parameters);

	if (0.0 < p[4] && p[4] < 8000.0) minMass = p[4];
	if (0.0 < p[5] && p[5] < 8000.0) maxMass = p[5];

	return kernel->Integral(minMass, maxMass, parameters, 1.0e-12);
}

/**
 * Function that is used in the SmearedAnsatz-function. In a sense this is it's kernel.
 *
 * @param x Pointer to the function's variable.
 * @param p Array of the function's parameters.
 *
 * @return Value of the function.
 */
double SmearedAnsatzKernel(double* x, double* p) {

	const double mass = x[0]; 		// True mass
	const double massMeas = p[0]; 		// Measured mass
	const double eta = p[1]; 		// Rapidity
	const double resolution = MassResolution(mass, eta+1.0e-3) * mass;

	const double s = TMath::Gaus(massMeas, mass, resolution, true);
	const double f = p[2] * std::pow(mass, p[3]) * std::pow(1.0 - mass/8000.0, p[4]);

	return f * s;
}

/**
 * Function that unfolds a histogram.
 *
 * @param massHist Histogram to be unfolded.
 * @param nloHist Corresponding theory prediction.
 * @param minEta Used as a starting parameter in a fit.
 *
 * @return A tuple that contains the unfolded mass histogram and graph.
 */
std::tuple<TH1D*,TGraphErrors*> UnfoldHistograms::UnfoldMassHistogram(TH1D* massHist, TH1D* nloHist, const double minEta) {

	Config& config = Config::GetInstance();
	std::vector<double> massRange = config.GetDoubleVector("massRange");

	// Function for fitting the data without smearing
	TF1 nloFunc("nloFunc", "[0]*pow(x,[1])*pow(1-x/8000.,[2])", massRange[0], massRange[1]);
	nloFunc.SetParameters(1.0e8, -5.0, 10.0);
	nloHist->Fit(&nloFunc, "QRN");

	// Graph of theory points with centered bins
	const double minErr = 0.02;
	TGraphErrors nloGraph(0);
	nloGraph.SetName("nloGraph");
	for (int bin = 1; bin != nloHist->GetNbinsX()+1; bin++) {
		const double y = nloHist->GetBinContent(bin);
		const double yErr = nloHist->GetBinError(bin);
		const double minMass = nloHist->GetBinLowEdge(bin);
		const double maxMass = nloHist->GetBinLowEdge(bin+1);

		const double y0 = nloFunc.Integral(minMass, maxMass) / (maxMass - minMass);
		const double x = nloFunc.GetX(y0, minMass, maxMass);

		nloGraph.SetPoint(bin-1, x, y);
		nloGraph.SetPointError(bin-1, 0.0, std::sqrt(std::pow(yErr,2.0) + std::pow(minErr*y,2.0)));
	}

	// Second fit to properly centered graph
	nloGraph.Fit(&nloFunc, "QRN");

	// Bin-centered data points
	TGraphErrors massGraph(0);
	massGraph.SetName("massGraph");
	for (int bin = 1; bin != nloHist->GetNbinsX()+1; bin++) {
		const double minMass = massHist->GetBinLowEdge(bin);
		const double maxMass = massHist->GetBinLowEdge(bin+1);
		const double y = nloFunc.Integral(minMass, maxMass) / (maxMass - minMass);
		const double x = nloFunc.GetX(y, minMass, maxMass);
		const double yMass = massHist->GetBinContent(bin);
		const double yMassErr = massHist->GetBinError(bin);
		if (yMass > 0.0) {
			massGraph.SetPoint(bin-1, x, yMass);
			massGraph.SetPointError(bin-1, 0.0, yMassErr);
		}
	}

	// Create a smeared theory curve
	TF1 nloSmearedFunc("nloSmearedFunc", SmearedAnsatz, massRange[0], massRange[1]-100.0, 7);
	nloSmearedFunc.SetParameters(minEta, nloFunc.GetParameter(0), nloFunc.GetParameter(1), nloFunc.GetParameter(2), 0.0, 0.0);

	// Deduce range and binning for true and measured spectra
	std::vector<double> realBinning;
	std::vector<double> measuredBinning;
	for (int bin = 1; bin != massHist->GetNbinsX()+1; bin++) {
		const double x = massHist->GetBinCenter(bin);
		const double xLow = massHist->GetBinLowEdge(bin);
		const double xHigh = massHist->GetBinLowEdge(bin+1);
		const double y = massHist->GetBinContent(bin);

		// No idea where these hardcoded numbers come from
		if (x >= 43.0 && y > 0.0) {
			if (realBinning.empty()) realBinning.push_back(xLow);
			realBinning.push_back(xHigh);
		}
		if (x >= 43.0 && y > 0.0) {
			if (measuredBinning.empty()) measuredBinning.push_back(xLow);
			measuredBinning.push_back(xHigh);
		}
	}

	// Copy over the relevant part of massHist
	TH1D recoHist("recoHist", "", measuredBinning.size()-1, &measuredBinning[0]);
	for (int bin = 1; bin != recoHist.GetNbinsX()+1; bin++) {
		const int massBin = massHist->FindBin(recoHist.GetBinCenter(bin));
		recoHist.SetBinContent(bin, massHist->GetBinContent(massBin));
		recoHist.SetBinError(bin, massHist->GetBinError(massBin));
	}

	TH2D mt("mt", "mt;m_{reco};m_{gen}", measuredBinning.size()-1, &measuredBinning[0], realBinning.size()-1, &realBinning[0]);
	TH1D mx("mx", "mx;m_{gen};#sigma/binWidth", realBinning.size()-1, &realBinning[0]);
	TH1D my("my", "my;m_{reco};#sigma/binWidth", measuredBinning.size()-1, &measuredBinning[0]);
	for (int xBin = 1; xBin != mt.GetNbinsX()+1; xBin++) {
		const double recoMassLow = mt.GetXaxis()->GetBinLowEdge(xBin);
		const double recoMassHigh = mt.GetXaxis()->GetBinLowEdge(xBin+1);
		const double yReco = nloFunc.Integral(recoMassLow, recoMassHigh) / (recoMassHigh - recoMassLow);
		const double massReco = nloFunc.GetX(yReco, recoMassLow, recoMassHigh);
		for (int yBin = 1; yBin != mt.GetNbinsY()+1; yBin++) {
			const double genMassLow = mt.GetYaxis()->GetBinLowEdge(yBin);
			const double genMassHigh = mt.GetYaxis()->GetBinLowEdge(yBin+1);
			// More magical low limits
			if (genMassLow > 50.0 && massReco > 50.0 && genMassLow < 8000.0) {
				nloSmearedFunc.SetParameter(4, genMassLow);
				nloSmearedFunc.SetParameter(5, genMassHigh);
				mt.SetBinContent(xBin, yBin, nloSmearedFunc.Eval(massReco) * (recoMassHigh - recoMassLow));
			}
		}
	}

	for (int yBin = 1; yBin != mt.GetNbinsY()+1; yBin++) {
		const double genMassLow = std::min(8000.0, mt.GetYaxis()->GetBinLowEdge(yBin));
		const double genMassHigh = std::min(8000.0, mt.GetYaxis()->GetBinLowEdge(yBin+1));
		const double yGen = nloFunc.Integral(genMassLow, genMassHigh);
		mx.SetBinContent(yBin, yGen);
	}
	for (int xBin = 1; xBin != mt.GetNbinsX()+1; xBin++) {
		double reco = 0.0;
		for (int yBin = 1; yBin != mt.GetNbinsY()+1; yBin++) {
			reco += mt.GetBinContent(xBin, yBin);
		}
		my.SetBinContent(xBin, reco);
	}
	// What the hell does "mtu" mean? These non-informative names are insanely frustrating.
	TH2D* mtu = (TH2D*)mt.Clone("mtu");
	for (int xBin = 1; xBin < mt.GetNbinsX()+1; xBin++) {
		for (int yBin = 1; yBin < mt.GetNbinsY()+1; yBin++) {
			if (mx.GetBinContent(yBin) != 0.0) {
				mtu->SetBinContent(xBin, yBin, mt.GetBinContent(xBin,yBin) / mx.GetBinContent(yBin));
			}
		}
	}

	TH2D mts("mts", "mts;m_{reco};m_{gen}", measuredBinning.size()-1, &measuredBinning[0], measuredBinning.size()-1, &measuredBinning[0]);
	TH1D mxs("mxs", "mxs;m_{gen};#sigma/binWidth", measuredBinning.size()-1, &measuredBinning[0]);
	for (int xBin = 1; xBin != mts.GetNbinsX()+1; xBin++) {
		for (int yBin = 1; yBin != mts.GetNbinsY()+1; yBin++) {
			const double x = mts.GetBinCenter(xBin);
			const double y = mts.GetBinCenter(yBin);
			const int xBin2 = mt.GetXaxis()->FindBin(x);
			const int yBin2 = mt.GetYaxis()->FindBin(y);
			mts.SetBinContent(xBin, yBin, mt.GetBinContent(xBin2, yBin2));
			mts.SetBinError(xBin, yBin, mt.GetBinError(xBin2, yBin2));
		}
	}
	for (int bin = 1; bin != mxs.GetNbinsX()+1; bin++) {
		const double x = mxs.GetBinCenter(bin);
		const int bin2 = mx.FindBin(x);
		mxs.SetBinContent(bin, mx.GetBinContent(bin2));
		mxs.SetBinError(bin, mx.GetBinError(bin2));
	}

	// Actual unfolding with the d'Agostini method
	RooUnfoldResponse uResp(&my, &mx, &mt);
	RooUnfoldBayes uBayes(&uResp, &recoHist, 4);

	TH1D* realBayesHist = (TH1D*)uBayes.Hreco(RooUnfold::kCovariance);
	TMatrixD covMat(uBayes.Ereco());

	TH1D* unfoldedMassHist = (TH1D*)massHist->Clone("unfoldedMassHistogram");
	unfoldedMassHist->Reset();
	for (int bin = 1; bin != realBayesHist->GetNbinsX()+1; bin++) {
		const int bin2 = massHist->FindBin(realBayesHist->GetBinCenter(bin));
		unfoldedMassHist->SetBinContent(bin2, realBayesHist->GetBinContent(bin));
		unfoldedMassHist->SetBinError(bin2, realBayesHist->GetBinError(bin));
	}

	RooUnfoldResponse uResps(&my, &mxs, &mts);
	RooUnfoldBinByBin uBin(&uResps, &recoHist);

	// Normalize unfoldedMassHist
	for (int bin = 1; bin != unfoldedMassHist->GetNbinsX()+1; bin++) {
		unfoldedMassHist->SetBinContent(bin, unfoldedMassHist->GetBinContent(bin));
		unfoldedMassHist->SetBinError(bin, unfoldedMassHist->GetBinError(bin));
	}

	// Make graphs
	TGraphErrors* unfoldedMassGraph = new TGraphErrors(0);
	unfoldedMassGraph->SetName("unfoldedMassGraph");
	for (int point = 0; point != massGraph.GetN(); point++) {
		double x, y;
		massGraph.GetPoint(point, x, y);
		const double xErr = massGraph.GetErrorX(point);
		const int bin2 = unfoldedMassHist->FindBin(x);
		const double yCorr = unfoldedMassHist->GetBinContent(bin2);
		const double yCorrErr = unfoldedMassHist->GetBinError(bin2);
		
		if (yCorr > 0.0) {
			const int nPoints = unfoldedMassGraph->GetN();
			unfoldedMassGraph->SetPoint(nPoints, x, yCorr);
			unfoldedMassGraph->SetPointError(nPoints, xErr, yCorrErr);
		}
	}

	return std::make_tuple(unfoldedMassHist, unfoldedMassGraph);
}
