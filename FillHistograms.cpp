#include <algorithm>
#include <cmath>
#include <fstream>
#include <map>
#include <sstream>

#include <chrono>

#include <TAxis.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>
#include <TGraphErrors.h>

#include "Config.hpp"
#include "DataWrapper.hpp"
#include "DuplicateRemover.hpp"
#include "FillHistograms.hpp"
#include "Logger.hpp"
#include "ProgressIndicator.hpp"
#include "Trigger.hpp"
#include "Utils.hpp"

#include "CondFormats/JetMETObjects/interface/FactorizedJetCorrector.h"
#include "CondFormats/JetMETObjects/interface/JetCorrectorParameters.h"

/**
 * Empty constructor.
 */
FillHistograms::FillHistograms() {
}

/**
 * Empty destructor.
 */
FillHistograms::~FillHistograms() {
}

/**
 * A method that loops over data and fills histograms.
 *
 * This method is called by the user when histograms need to be filled. Data file paths
 * are loaded from the configuration file parameter "dataFileList". These files are chained up
 * and looped through while reading relevant data fields and processing them into histograms.
 * Also Monte Carlo data is processed. Monte Carlo data file paths are read from
 * the configuration file parameter "mcFileList".
 */
void FillHistograms::Fill() {

	CompositeLogger& logger = CompositeLogger::GetInstance();
	Config& config = Config::GetInstance();

	// Initialize histograms
	TH1::SetDefaultSumw2(kTRUE);
	InitializeHistograms();

	// Initialize wrappers
	const std::vector<std::string> dataFiles = config.GetStringVector("dataFileList");
	const std::vector<std::string> mcFiles = config.GetStringVector("mcFileList");
	DataWrapper data(dataFiles, false);
	DataWrapper mc(mcFiles, true);

	// Fill histograms
	logger << DEBUG << "Looping over data.\n";
	Loop(data, false);
	logger << DEBUG << "Looping over MC.\n";
	Loop(mc, true);
}

/**
 * Writes histograms to disk.
 *
 * This method writes the previously filled histograms to a ROOT-file.
 * Subdirectories are created for each eta bin and trigger.
 *
 * @param filename Path to the ROOT-file that will be written to.
 */
void FillHistograms::Write(std::string filename) {

	TFile file(filename.c_str(), "recreate");
	for (auto& array : mHistogramWrappers) {
		for (auto& elem : array.second) {
			elem.second.Write();
		}
	}
	for (auto& elem : mMonteCarloWrappers) {
		elem.second.Write();
	}
	file.Close();
}

/**
 * Applies jet corrections.
 *
 * This method is used to apply jet corrections to the four momenta of the 
 * leading jets. Works for both data and Monte Carlo.
 *
 * @param rho Rho value for the jet.
 * @param area Area for the jet.
 * @param p4 Four momentum of the jet. Corrections are applied to this variable.
 * @param isMC Whether this jet is from data or Monte Carlo.
 */
void FillHistograms::ApplyCorrections(float rho, float area, TLorentzVector& p4, bool isMC) {

	// Corrections used for data jets.
	static std::vector<JetCorrectorParameters> dataCorrectorParams = {
		JetCorrectorParameters("CondFormats/JetMETObjects/data/FT_53_V21_AN4_L1FastJet_AK5PF.txt"),
		JetCorrectorParameters("CondFormats/JetMETObjects/data/FT_53_V21_AN4_L2Relative_AK5PF.txt"),
		JetCorrectorParameters("CondFormats/JetMETObjects/data/FT_53_V21_AN4_L3Absolute_AK5PF.txt"),
		JetCorrectorParameters("CondFormats/JetMETObjects/data/FT_53_V21_AN4_L2L3Residual_AK5PF.txt")
	};
	static FactorizedJetCorrector sDataJec(dataCorrectorParams);
	// Corrections used for Monte Carlo jets.
	static std::vector<JetCorrectorParameters> mcCorrectorParams = {
		JetCorrectorParameters("CondFormats/JetMETObjects/data/FT_53_V21_AN4_L1FastJet_AK5PF.txt"),
		JetCorrectorParameters("CondFormats/JetMETObjects/data/FT_53_V21_AN4_L2Relative_AK5PF.txt"),
		JetCorrectorParameters("CondFormats/JetMETObjects/data/FT_53_V21_AN4_L3Absolute_AK5PF.txt")
	};
	static FactorizedJetCorrector sMCJec(mcCorrectorParams);

	FactorizedJetCorrector* jec = isMC ? &sMCJec : &sDataJec;
	jec->setRho(rho);
	jec->setJetA(area);
	jec->setJetPt(p4.Pt());
	jec->setJetEta(p4.Eta());

	// Apply the correction
	p4 *= jec->getCorrection();
}

/**
 * Initializes the underlying wrappers for histograms.
 *
 * Each histogram is divided by eta bin and trigger into a histogram wrapper.
 * These histogram wrappers are initialized in this method for both data and
 * Monte Carlo. Wrappers contain each relevant histogram, profile, graph, etc
 * and information about their location that is, a directory, in the future ROOT-file.
 */
void FillHistograms::InitializeHistograms() {

	Config& config = Config::GetInstance();
	const std::vector<double> etaBins = config.GetDoubleVector("etaBins");
	const std::vector<std::string> triggerNames = config.GetStringVector("triggerNames");
	const std::vector<int> useTriggers = config.GetIntegerVector("useTriggers");
	const std::vector<double> massRange = config.GetDoubleVector("massRange");

	const int numBins = config.GetInteger("numMassBins");
	const int minMass = massRange[0];
	const int maxMass = massRange[1];
	std::vector<double> bins;
	for (double m = std::log10(minMass); m <= std::log10(maxMass); m += (std::log10(maxMass)-std::log10(minMass))/numBins) {
		bins.push_back(std::pow(10.0, m));
	}

	// Wrappers for data histograms
	std::stringstream ss;
	for (unsigned int iEta = 0; iEta != etaBins.size()-1; iEta++) {
		// This little rain dance with directories is needed to avoid warnings from ROOT.
		// Warnings are caused by the retarded way that ROOT uses to handle IO.
		ss.str("");
		ss << "Eta_" << etaBins[iEta] << "-" << etaBins[iEta+1];
		const std::string& etaDir = ss.str();
		gDirectory->mkdir(etaDir.c_str())->cd();
		for (unsigned int iTrg = 0; iTrg != triggerNames.size(); iTrg++) {
			// Initialize only if the trigger is marked to be used
			if (Utils::Exists(useTriggers, (int)iTrg)) {
				gDirectory->mkdir(triggerNames[iTrg].c_str())->cd();
				// Generate directory name for the wrapper
				ss.str("");
				ss << etaDir << "/" << triggerNames[iTrg];
				// Create the wrapper
				auto& wrapper = mHistogramWrappers[iEta][iTrg];
				wrapper.SetDirectory(ss.str());
				// Add necessary histograms into it
				wrapper.AddHistogram("massHistogram", "Mass spectrum histogram", numBins, &bins[0]);
				wrapper.AddProfile("massProfile", "Mass profile", numBins, &bins[0]);
				wrapper.AddGraph("massGraph", "Mass graph");
				gDirectory->cd("..");
			}
		}
		gDirectory->cd("..");
	}

	// Wrappers for the Monte Carlo histograms
	for (unsigned int iEta = 0; iEta != etaBins.size()-1; iEta++) {
		ss.str("");
		ss << "Eta_" << etaBins[iEta] << "-" << etaBins[iEta+1];
		const std::string dirName = ss.str();
		gDirectory->cd(dirName.c_str());
		gDirectory->mkdir("mc")->cd();
		// Create the wrapper
		auto& wrapper = mMonteCarloWrappers[iEta];
		wrapper.SetDirectory(dirName + "/mc");
		// Add contents
		wrapper.AddHistogram("massHistogram", "Mass spectrum histogram", numBins, &bins[0]);
		wrapper.AddProfile("massProfile", "Mass profile", numBins, &bins[0]);
		wrapper.AddGraph("massGraph", "Mass graph");
		wrapper.AddHistogram("genMassHistogram", "Mass spectrum histogram", numBins, &bins[0]);
		wrapper.AddProfile("genMassProfile", "Mass profile", numBins, &bins[0]);
		wrapper.AddGraph("genMassGraph", "Mass graph");
		wrapper.AddHistogram2D("massResHistogram", "", numBins, &bins[0], 300, &Utils::LinSpace(-1.0, 3.0, 300)[0]);
		gDirectory->cd("../..");
	}
}

/**
 * Loops through data and fills histograms.
 *
 * This is the method where the actual looping through data happens.
 * Data is pulled from input files entry by entry, corrections are applied
 * and the computed dijet mass is added to the appropriate histogram.
 *
 * @param data A data wrapper that contains the current data fields.
 * @param isMC Whether the data wrapper represents real data or Monte Carlo.
 */
void FillHistograms::Loop(DataWrapper& data, bool isMC) {

	CompositeLogger& logger = CompositeLogger::GetInstance();

	const long int numEntries = data.GetEntries();
	logger << DEBUG << "Input data contains " << numEntries << " entries in total.\n";

	// Get some variables from config
	Config& config = Config::GetInstance();
	std::vector<double> etaBins = config.GetDoubleVector("etaBins");
	std::vector<std::string> triggerNames = config.GetStringVector("triggerNames");
	const std::vector<int> useTriggers = config.GetIntegerVector("useTriggers");
	bool checkForDuplicates = config.GetBoolean("checkForDuplicates");

	TLorentzVector v1, v2, v1Gen, v2Gen;
	long int lastProgress = 0;
	long int progressInterval = 100000;
	std::set<Trigger> firedTriggers;
	ProgressIndicator progressIndicator(numEntries);
	progressIndicator.Start();
	DuplicateRemover* duplicateRemover = checkForDuplicates ? new HashedRemover : nullptr;

	unsigned long int inputMicros = 0;
	unsigned long int cpuMicros = 0;
	auto t1 = std::chrono::system_clock::now(); 			// Used for measuring the disk io vs cpu time ratio
	auto t2 = std::chrono::system_clock::now();

	// Loop over all events
	for (Long64_t entry = 0; entry != numEntries; entry++) {

		// Print progress
		if (lastProgress + progressInterval < entry) {
			progressIndicator.SetProgress(entry);
			std::cout << progressIndicator.GetProgressBar() << '\r' << std::flush;
	//		std::cout << 1.0*entry/numEntries << " " << progressIndicator.GetElapsed() << std::endl;
			lastProgress = entry;
		}

		t1 = std::chrono::system_clock::now();
		cpuMicros += std::chrono::duration_cast<std::chrono::microseconds>(t1-t2).count();
		// Read branches
		data.GetEntry(entry);
		t2 = std::chrono::system_clock::now();
		inputMicros += std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();

		// Accept only events with at least 2 jets
		if (data.numJets < 2) continue;

		// Check if event is duplicate
		if (checkForDuplicates && !duplicateRemover->AddIfUnique(data.run, data.lumisect, data.event)) continue;

		// Check which triggers fired
		firedTriggers.clear();
		for (unsigned int iTrg = 0; iTrg != data.triggerDecision.size(); iTrg++) {
			if (data.triggerDecision[iTrg] == 1) {
				const Trigger trigger = Utils::IndexToTrigger(iTrg);
				if (Utils::Exists(useTriggers, (int)trigger)) {
					firedTriggers.insert(trigger);
				}
			}
		}

		// Fill mass histograms
		if (isMC) {
			FillMCMass(data);
		} else {
			FillDataMass(data, firedTriggers);
		}
	}

	if (isMC) {
		FillMCAdditional();
	} else {
		FillDataAdditional();
	}

	logger << "inputMicros = " << inputMicros << "\n";
	logger << "cpuMicros = " << cpuMicros << "\n";
	logger << "input/cpu = " << 1.0*inputMicros/cpuMicros << "\n";
	if (checkForDuplicates) {
		logger << "duplicates = " << duplicateRemover->GetNumDuplicates() << "\n";
		delete duplicateRemover;
	}
}

/**
 * Additional histograms, profiles etc are filled here.
 *
 * This method is meant to be run after the data histograms are filled.
 * Here the mass graph is built for each eta bin and trigger based on
 * the mass histograms and profiles filled previously.
 */
void FillHistograms::FillDataAdditional() {

	for (auto& array : mHistogramWrappers) {
		for (auto& elem : array.second) {
			auto& wrapper = elem.second;
			auto massHistogram = (TH1D*)wrapper.Get("massHistogram");
			auto massProfile = (TProfile*)wrapper.Get("massProfile");
			auto massGraph = (TGraphErrors*)wrapper.Get("massGraph");
			for (int bin = 1; bin != massHistogram->GetNbinsX()+1; bin++) {
				const double x = massProfile->GetBinContent(bin);
				const double y = massHistogram->GetBinContent(bin);
				if (x > 0.0 && y > 0.0) {
					const int point = massGraph->GetN();
					massGraph->SetPoint(point, x, y);
					massGraph->SetPointError(point, massProfile->GetBinError(bin), massProfile->GetBinError(bin));
				}
			}
			massGraph->GetXaxis()->SetLimits(10.0, 10000.0);
			massGraph->GetHistogram()->SetMinimum(2.0e-7);
			massGraph->GetHistogram()->SetMaximum(5.0e3);
		}
	}
}

/**
 * Additional histograms, profiles etc are filled here for MC.
 *
 * This method is the same as the FillHistograms::FillDataAdditional -method
 * except for Monte Carlo instead of real data. The only difference is that
 * MC has only eta bins not triggers and MC has also data from the
 * generator (prefixed with "gen").
 */
void FillHistograms::FillMCAdditional() {

	for (auto& elem : mMonteCarloWrappers) {
		auto& wrapper = elem.second;
		auto massHistogram = (TH1D*)wrapper.Get("massHistogram");
		auto massProfile = (TProfile*)wrapper.Get("massProfile");
		auto massGraph = (TGraphErrors*)wrapper.Get("massGraph");
		auto genMassHistogram = (TH1D*)wrapper.Get("genMassHistogram");
		auto genMassProfile = (TProfile*)wrapper.Get("genMassProfile");
		auto genMassGraph = (TGraphErrors*)wrapper.Get("genMassGraph");
		for (int bin = 1; bin != massHistogram->GetNbinsX()+1; bin++) {
			const double x = massProfile->GetBinContent(bin);
			const double y = massHistogram->GetBinContent(bin);
			if (x > 0.0 && y > 0.0) {
				const int point = massGraph->GetN();
				massGraph->SetPoint(point, x, y);
				massGraph->SetPointError(point, massProfile->GetBinError(bin), massProfile->GetBinError(bin));
			}
			const double genX = genMassProfile->GetBinContent(bin);
			const double genY = genMassHistogram->GetBinContent(bin);
			if (genX > 0.0 && genY > 0.0) {
				const int point = genMassGraph->GetN();
				genMassGraph->SetPoint(point, genX, genY);
				genMassGraph->SetPointError(point, genMassProfile->GetBinError(bin), genMassProfile->GetBinError(bin));
			}
		}
		massGraph->GetXaxis()->SetLimits(10.0, 10000.0);
		massGraph->GetHistogram()->SetMinimum(2.0e-7);
		massGraph->GetHistogram()->SetMaximum(5.0e3);
	}
}

/**
 * Fill mass histogram and profile for data.
 *
 * Takes data from a single data entry, applies jet corrections, computes dijet mass
 * and adds it into the mass histogram and profile.
 *
 * @param data A data wrapper that contains the current data fields.
 * @param firedTriggers A set of triggers that fired for this data entry.
 */
void FillHistograms::FillDataMass(DataWrapper& data, const std::set<Trigger> firedTriggers) {

	static Config& config = Config::GetInstance();
	static const std::vector<double> etaBins = config.GetDoubleVector("etaBins");
	static const std::vector<std::string> useTriggers = config.GetStringVector("useTriggers");
	static const int numEtaBins = (int)etaBins.size();
	static TLorentzVector v1, v2;
	static double mass;
	static int etaBin;

	v1.SetPxPyPzE(data.p4x[0], data.p4y[0], data.p4z[0], data.p4t[0]);
	v2.SetPxPyPzE(data.p4x[1], data.p4y[1], data.p4z[1], data.p4t[1]);

	ApplyCorrections(data.rho, data.area[0], v1, false);
	ApplyCorrections(data.rho, data.area[1], v2, false);
	mass = (v1+v2).M();

	etaBin = Utils::FindBin(etaBins, std::max(std::abs(v1.Eta()), std::abs(v2.Eta())));
	if (0 <= etaBin && etaBin < numEtaBins-1) {
		for (Trigger trig : firedTriggers) {
			const auto trigString = Utils::TriggerToString(trig);
			auto& wrapper = mHistogramWrappers[etaBin][trig];
			((TH1D*)wrapper.Get("massHistogram"))->Fill(mass);
			((TProfile*)wrapper.Get("massProfile"))->Fill(mass, mass);
		}
	}
}

/**
 * Fill mass histogram, profile and resolution for Monte Carlo.
 *
 * Same thing as the FillHistograms::FillDataMass -method except for Monte Carlo
 * instead of real data. The difference is again that MC also has generated
 * data (prefixed with "gen"). Also this method fills the resolution 2d-histogram.
 *
 * @param data A data wrapper that contains the current data fields.
 */
void FillHistograms::FillMCMass(DataWrapper& data) {

	static const std::vector<double> etaBins = Config::GetInstance().GetDoubleVector("etaBins");
	static const int numEtaBins = (int)etaBins.size();
	static TLorentzVector v1, v2, v1Gen, v2Gen;
	static double mass, genMass;
	static int etaBin;

	v1.SetPxPyPzE(data.p4x[0], data.p4y[0], data.p4z[0], data.p4t[0]);
	v2.SetPxPyPzE(data.p4x[1], data.p4y[1], data.p4z[1], data.p4t[1]);
	v1Gen.SetPxPyPzE(data.genP4x[0], data.genP4y[0], data.genP4z[0], data.genP4t[0]);
	v2Gen.SetPxPyPzE(data.genP4x[1], data.genP4y[1], data.genP4z[1], data.genP4t[1]);

	if (data.radius[0] < 0.5 && data.radius[1] < 0.5) {
		genMass = (v1Gen+v2Gen).M();
		etaBin = Utils::FindBin(etaBins, std::max(std::abs(v1Gen.Eta()), std::abs(v2Gen.Eta())));
		if (0 <= etaBin && etaBin < numEtaBins-1) {
			auto& wrapper = mMonteCarloWrappers[etaBin];
			((TH1D*)wrapper.Get("genMassHistogram"))->Fill(genMass, data.weight);
			((TProfile*)wrapper.Get("genMassProfile"))->Fill(genMass, genMass);
		}
	} else {
		genMass = 0.0;
	}

	ApplyCorrections(data.rho, data.area[0], v1, true);
	ApplyCorrections(data.rho, data.area[1], v2, true);
	mass = (v1+v2).M();
	etaBin = Utils::FindBin(etaBins, std::max(std::abs(v1.Eta()), std::abs(v2.Eta())));
	if (0 <= etaBin && etaBin < numEtaBins-1) {
		auto& wrapper = mMonteCarloWrappers[etaBin];
		((TH1D*)wrapper.Get("massHistogram"))->Fill(mass, data.weight);
		((TProfile*)wrapper.Get("massProfile"))->Fill(mass, mass);
		if (genMass > 0.0 && std::abs(v1.Pt()/v1Gen.Pt()-1.0) < 0.5 && std::abs(v2.Pt()/v2Gen.Pt()-1.0) < 0.5) {
			((TH2D*)wrapper.Get("massResHistogram"))->Fill(genMass, (mass-genMass)/genMass, data.weight);
		}
	}
}

