#ifndef _COMBINE_HISTOGRAMS_HPP
#define _COMBINE_HISTOGRAMS_HPP

#include <string>
#include <unordered_map>

#include <TH1D.h>
#include <TProfile.h>
#include <TGraphErrors.h>

#include "HistogramWrapper.hpp"

/**
 * Class for combining histograms from different triggers.
 *
 * This class handles the combining of histograms. In earlier parts of the analysis
 * chain, the real data histograms are divided by trigger in each eta bin. This class
 * takes those trigger histograms, profiles and graphs and cuts/pastes them at predefined
 * mass intervals into a combined histogram, profile and graph. This is done for each
 * eta bin. The intervals at which the triggers are combined is determined in the 
 * configuration file and accessed through the Conf-singleton.
 */
class CombineHistograms {
public:
	void Combine(std::string filename);
	void WriteHistograms(std::string filename);
private:
	TH1D* CombineTriggerHistograms(std::string name, std::string title, std::unordered_map<int,TH1D*> triggerHistograms);
	TProfile* CombineTriggerProfiles(std::string name, std::string title, std::unordered_map<int,TProfile*> triggerProfiles);
	void LoadFromFile(std::string filename);

	std::unordered_map<int,std::unordered_map<int,HistogramWrapper>> mHistogramWrappers;
	std::unordered_map<int,HistogramWrapper> mMonteCarloWrappers;
	std::unordered_map<int,HistogramWrapper> mCombinedWrappers;
};

#endif

