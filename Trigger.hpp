#ifndef _TRIGGER_HPP
#define _TRIGGER_HPP

#include <string>

enum Trigger {

	jt40 = 0,
	jt80 = 1,
	jt140 = 2,
	jt200 = 3,
	jt260 = 4,
	jt320 = 5,
	jt400 = 6
};

#endif
