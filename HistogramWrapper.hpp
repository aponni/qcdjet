#ifndef _HISTOGRAM_WRAPPER_HPP
#define _HISTOGRAM_WRAPPER_HPP

#include <tuple>
#include <string>
#include <stdexcept>
#include <unordered_map>

#include <TObject.h>

/**
 * Class that wraps multiple ROOT-objects into a single object.
 * 
 * This class is used to wrap a number of histograms, profiles,
 * graphs and other ROOT-objects into a single structure.
 * The intent is to increase the level of abstraction by
 * e.g. placing all objects associated with a single eta bin
 * and trigger in a same wrapper. These objects can be retrieved
 * from the wrapper by their name.
 * HistogramWrapper also handles the writing of objects associated
 * with it to the disk. This is done simply by calling Write().
 * The Write()-method creates automatically the appropriate
 * directory structure in the ROOT-file and places it's objects
 * into it.
 */
class HistogramWrapper {
public:
	HistogramWrapper();
	HistogramWrapper(std::string directory);
	~HistogramWrapper();
	void AddHistogram(std::string name, std::string title, int numBins, double* bins, bool doWrite = true, bool doDelete = true);
	void AddHistogram2D(std::string name, std::string title, int numBinsX, double* binsX, int numBinsY, double* binsY, bool doWrite = true, bool doDelete = true);
	void AddProfile(std::string name, std::string title, int numBins, double* bins, bool doWrite = true, bool doDelete = true);
	void AddGraph(std::string name, std::string title, bool doWrite = true, bool doDelete = true);
	void AddObject(TObject* obj, bool doWrite = true, bool doDelete = true);
	inline TObject* Get(const std::string& name) {
		TObject* obj = std::get<0>(mObjects[name]);
		if (obj == nullptr) {
			throw std::runtime_error((name + " doesn't exist.").c_str());
		}
		return obj;
	}
	void SetDirectory(std::string directory);
	void Write();
private:
	std::string mDirectory;
	std::unordered_map<std::string, std::tuple<TObject*,bool,bool>> mObjects;
};

#endif
