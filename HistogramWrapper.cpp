#include <cmath>
#include <sstream>

#include <TDirectory.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>
#include <TGraphErrors.h>

#include "Config.hpp"
#include "HistogramWrapper.hpp"
#include "Utils.hpp"


/**
 * Constructor.
 */
HistogramWrapper::HistogramWrapper() {

	// Setting a default path to catch possible errors.
	// The directory should always be set with SetDirectory() or the alternative constructor.
	mDirectory = "no_eta/no_trig";
}

/**
 * Constructor thaConstructor thatt
 */
HistogramWrapper::HistogramWrapper(std::string directory) {

	mDirectory = directory;
}

/**
 *
 */
HistogramWrapper::~HistogramWrapper() {

	// TODO: Figure out why this explodes
	// Maybe while copy constructing, the previous instance deletes pointers that are still held by the new instance
/*	for (auto entry : mObjects) {
		if (std::get<2>(entry.second)) {
			delete std::get<0>(entry.second);
		}
	}*/
}

/**
 *
 */
void HistogramWrapper::AddHistogram(std::string name, std::string title, int numBins, double* bins, bool doWrite, bool doDelete) {

	TH1D* histogram = new TH1D(name.c_str(), title.c_str(), numBins, bins);
	mObjects[name.c_str()] = std::make_tuple(histogram, doWrite, doDelete);
}

/**
 *
 */
void HistogramWrapper::AddHistogram2D(std::string name, std::string title, int numBinsX, double* binsX, int numBinsY, double* binsY, bool doWrite, bool doDelete) {

	TH2D* histogram2D = new TH2D(name.c_str(), title.c_str(), numBinsX, binsX, numBinsY, binsY);
	mObjects[name.c_str()] = std::make_tuple(histogram2D, doWrite, doDelete);
}

/**
 *
 */
void HistogramWrapper::AddProfile(std::string name, std::string title, int numBins, double* bins, bool doWrite, bool doDelete) {

	TProfile* profile = new TProfile(name.c_str(), title.c_str(), numBins, bins);
	mObjects[name.c_str()] = std::make_tuple(profile, doWrite, doDelete);
}

/**
 *
 */
void HistogramWrapper::AddGraph(std::string name, std::string title, bool doWrite, bool doDelete) {

	TGraphErrors* graph = new TGraphErrors(0);
	graph->SetName(name.c_str());
	mObjects[name.c_str()] = std::make_tuple(graph, doWrite, doDelete);
}

/**
 *
 */
void HistogramWrapper::AddObject(TObject* obj, bool doWrite, bool doDelete) {

	mObjects[obj->GetName()] = std::make_tuple(obj, doWrite, doDelete);
}

/**
 *
 */
void HistogramWrapper::SetDirectory(std::string directory) {

	mDirectory = directory;
}

/**
 *
 */
void HistogramWrapper::Write() {

	const std::vector<std::string> subDirs = Utils::SplitLine(mDirectory, { '/' });
	// Navigate to the directory
	for (const std::string& dir : subDirs) {
		if (gDirectory->GetDirectory(dir.c_str()) == nullptr) {
			gDirectory->mkdir(dir.c_str());
		}
		gDirectory->cd(dir.c_str());
	}

	// Write objects 
	for (auto& entry : mObjects) {
		if (std::get<1>(entry.second)) {
			std::get<0>(entry.second)->Write();
		}
	}

	// Navigate back
	for (const std::string& sub : subDirs) {
		gDirectory->cd("..");
	}
}

