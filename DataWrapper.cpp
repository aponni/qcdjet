#include "Config.hpp"
#include "DataWrapper.hpp"
#include "Logger.hpp"

/**
 * Constructor.
 *
 * A constructor for DataWrapper that initializes the wrapper for use:
 * input files are turned into a chain and branches are set up for reading.
 *
 * @param inputFiles A list of paths to input data files.
 * @param isMC Whether we are dealing with Monte Carlo or real data.
 */
DataWrapper::DataWrapper(std::vector<std::string> inputFiles, bool isMC) {

	// Build chain
	BuildChain(inputFiles);
	// Connect branches to variables
	ConnectVariables();
	// Enable relevant branches
	EnableBranches(isMC);
}

/**
 * Destructor.
 *
 * A destructor that cleans up the data chain.
 */
DataWrapper::~DataWrapper() {

	delete mChain;
}

/**
 * Populates member variables with data from the given entry.
 *
 * @param entry Number of the entry from which the data will be pulled into member variables.
 */
void DataWrapper::GetEntry(long int entry) {

	mChain->GetEntry(entry);
}

/**
 * Get the number of entries in the current data chain.
 *
 * @return Number of entries in the chain.
 */
long int DataWrapper::GetEntries() {

	return mChain->GetEntries();
}

/**
 * A method that builds a TChain from the given list of input files.
 *
 * @param inputFiles List of input file paths to be chained up.
 */
void DataWrapper::BuildChain(std::vector<std::string> inputFiles) {

	const std::string chainName = Config::GetInstance().GetString("chainName");
	mChain = new TChain(chainName.c_str());

	for (std::string file : inputFiles) {
		mChain->Add(file.c_str());
	}
}

/**
 * Connects branches to the appropriate member variables.
 */
void DataWrapper::ConnectVariables() {

	mChain->SetMakeClass(1);
	mChain->SetBranchAddress("PFJets_", &numJets, &numBranch);
	mChain->SetBranchAddress("EvtHdr_.mRun", &run, &runBranch);
	mChain->SetBranchAddress("EvtHdr_.mEvent", &event, &eventBranch);
	mChain->SetBranchAddress("EvtHdr_.mLumi", &lumisect, &lumisectBranch);
	mChain->SetBranchAddress("EvtHdr_.mPFRho", &rho, &rhoBranch);
	mChain->SetBranchAddress("EvtHdr_.mWeight", &weight, &weightBranch);
	mChain->SetBranchAddress("PFJets_.genR_", radius, &radiusBranch);
	mChain->SetBranchAddress("TriggerDecision_", &triggerDecision, &triggerBranch);
	mChain->SetBranchAddress("PFJets_.P4_.fCoordinates.fX", p4x, &p4xBranch);
	mChain->SetBranchAddress("PFJets_.P4_.fCoordinates.fX", p4y, &p4yBranch);
	mChain->SetBranchAddress("PFJets_.P4_.fCoordinates.fZ", p4z, &p4zBranch);
	mChain->SetBranchAddress("PFJets_.P4_.fCoordinates.fT", p4t, &p4tBranch);
	mChain->SetBranchAddress("PFJets_.genP4_.fCoordinates.fX", genP4x, &genP4xBranch);
	mChain->SetBranchAddress("PFJets_.genP4_.fCoordinates.fY", genP4y, &genP4yBranch);
	mChain->SetBranchAddress("PFJets_.genP4_.fCoordinates.fZ", genP4z, &genP4zBranch);
	mChain->SetBranchAddress("PFJets_.genP4_.fCoordinates.fT", genP4t, &genP4tBranch);
	mChain->SetBranchAddress("PFJets_.area_", area, &areaBranch);
}

/**
 * Enables only those branches that are really needed in order to
 * optimize reading performance.
 *
 * @param Whether we are using Monte Carlo or real data.
 */
void DataWrapper::EnableBranches(bool isMC) {

	mChain->SetBranchStatus("*",0);
	mChain->SetBranchStatus("PFJets_",1);
	mChain->SetBranchStatus("EvtHdr_.mRun",1);
	mChain->SetBranchStatus("EvtHdr_.mEvent",1);
	mChain->SetBranchStatus("EvtHdr_.mLumi",1);
	mChain->SetBranchStatus("EvtHdr_.mPFRho",1);
	mChain->SetBranchStatus("PFJets_.P4_.fCoordinates.fX",1);
	mChain->SetBranchStatus("PFJets_.P4_.fCoordinates.fY",1);
	mChain->SetBranchStatus("PFJets_.P4_.fCoordinates.fZ",1);
	mChain->SetBranchStatus("PFJets_.P4_.fCoordinates.fT",1);
	mChain->SetBranchStatus("PFJets_.area_",1);
	if (isMC) {
		mChain->SetBranchStatus("EvtHdr_.mWeight",1);
		mChain->SetBranchStatus("PFJets_.genR_",1);
		mChain->SetBranchStatus("PFJets_.genP4_.fCoordinates.fX",1);
		mChain->SetBranchStatus("PFJets_.genP4_.fCoordinates.fY",1);
		mChain->SetBranchStatus("PFJets_.genP4_.fCoordinates.fZ",1);
		mChain->SetBranchStatus("PFJets_.genP4_.fCoordinates.fT",1);
	} else {
		mChain->SetBranchStatus("TriggerDecision_",1);
	}
}
