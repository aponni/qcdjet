#ifndef _CONFIG_HPP
#define _CONFIG_HPP

#include <string>
#include <sstream>
#include <map>
#include <vector>

class Config {
public:
	Config(const Config&) = delete;
	Config& operator=(const Config&) = delete;
	static Config& GetInstance();
	bool GetBoolean(std::string option);
	int GetInteger(std::string option);
	double GetDouble(std::string option);
	std::string GetString(std::string option);
	std::vector<bool> GetBooleanVector(std::string option);
	std::vector<int> GetIntegerVector(std::string option);
	std::vector<double> GetDoubleVector(std::string option);
	std::vector<std::string> GetStringVector(std::string option);

private:
	Config();
	void SanityCheck();
	void Load(std::string confFile);
	bool ParseOption(std::stringstream& ss);
	bool ParseVectorOption(std::stringstream& ss);
	
	std::map<std::string,bool> mBooleanOptions;
	std::map<std::string,int> mIntegerOptions;
	std::map<std::string,double> mDoubleOptions;
	std::map<std::string,std::string> mStringOptions;

	std::map<std::string,std::vector<bool>> mBooleanVectorOptions;
	std::map<std::string,std::vector<int>> mIntegerVectorOptions;
	std::map<std::string,std::vector<double>> mDoubleVectorOptions;
	std::map<std::string,std::vector<std::string>> mStringVectorOptions;
	
};

#endif

